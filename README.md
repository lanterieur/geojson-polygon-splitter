# geojson-polygon-splitter



## Intro

A single HTML page tool to split a GEOJSON feature polygon.

Splits along parallel cut lines or cut lines crossing at the 'center'.

The intended use it to split buildings floor plans. There was no need to perform a proper sphere projection. The tool is simply scaling long,lat values to the x,y axes. 

## User guide

### Load source

- Paste a valid geojson feature object with a single polygon geometry into the source textbox.
- Click "Load Source"

![Loaded source](./README_MEDIA/load-source.png)

### Axis configuration

- The X axis is in Red.
- The Y axis is in Green.

- "Axes rotation" is the 0-180 degree angle rotating both axes counter-clockwise.

![Axes rotation](./README_MEDIA/v-axes-rotation.gif)

- "Intersection angle" is the 0-180 degree angle between the x and y axes.

![Intersection angle](./README_MEDIA/v-intersection-angle.gif)

- "Move center" allows the user to click to pick a new center location on the canvas.

![Moving center](./README_MEDIA/v-move.gif)
![Cancel move center](./README_MEDIA/v-move-cancel.gif)

- The "Only one axis" checkbox toggles the "one-axis" and "two-axes" modes.


### Cuts

Cuts lines are represented by narrow orange lines.

#### Two Axes

In two axes mode, cut lines all cross the center point.

- "YX cut lines" is the number of cut lines from the Y axis (including) to the X axis (excluding) counter clock wise. Lines are evenly angled from one another.
- "XY cut lines" is the number of cut lines from the X axis (including) to the Y axis (excluding) counter clock wise.

For both axes, the cut line #1 is on the axis. Zero means no cut line. One is only on the axis. More will divide the two opposite quadrants by the number of cut lines.

#### One Axis

In the one axis mode, cut lines are parallel to one another and evenly spaced.

- "Number of cuts" represents the number of cut lines.
- "Cut spacing" is the space between two cut lines. This is recomputed automatically when the number of cuts or the direction of cuts is changed. The default value corresponds to the distance between (x min, y min) and (x max, y max) divided by the number of cuts + one. Spacing can be adjusted manually but will be recomputed if other parameters change.
- "Cuts direction" is the orientation of the cuts relative to the X axis. Either parallel or perpendicular.

#### Output Format

The tool has two ouput formats:
- Geojson feature. It will return an array of clones of the source feature, one per new shape, with the geometry of the new shape.
- Room geometries. It will return an object of new shape geometries indexed by order of cut.

#### Cut

The tool creates cut points at all intersections between cut lines and the edge of the polygon.
In two axes mode when more than 2 cuts occur, only the two cut points closest to and opposite from the center point.

The tool then creates shapes:
- In one axis mode or when the center point is outside the polygon, shapes are slices of the polygon along cut lines.

![One axis cuts](./README_MEDIA/one-axis-cuts.png)
![One axis parallel slanted cuts](./README_MEDIA/slanted.png)

- In two axes mode when the center point is inside the polygon, the new shapes are like pizza slices meeting at the center point.

![Two axes cuts](./README_MEDIA/two-axes-cuts.png)
![Two axes pizza cuts](./README_MEDIA/pizza.png)

### Output

The output is set as value of the output textarea.

## Known issues:

- Tall geometries (delta y much greater than delta x) may clip at the bottom since scaling is based on the X axis. Resizing the browser window to a portrait aspect ratio may help.
- The page requires a reload to perform different cuts on the same geometry. I do not intent to fix this bug as reloading the page works well for my use case.
- Sometimes cuts just won't work on the first try.
- One axis cut spacing does not always recompute after manual input. Axis mode change or reload fixes it.
- The tool was made to cut hundreds of relatively simple buildings floor plans. It has not been tested with more complex geometries like spirals. Weird result may occur.